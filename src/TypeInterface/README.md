# Type Interfaces

Type interfaces describe different aspects of Types. At minimum, every Type must implement `Type`.

1. `Type` - The base Type definition.

## See also
* [Types](../Type)