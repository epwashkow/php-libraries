<?php

declare(strict_types=1);

namespace EvanWashkow\PhpLibraries\Collection;

use EvanWashkow\PhpLibraries\CollectionInterface\SettableCollector;
use EvanWashkow\PhpLibraries\TypeInterface\Type;

/**
 * Stores a sequential list of values by 0-based indexes
 *
 * @template TValue The value type
 *
 * @implements SettableCollector<int, TValue>
 */
final class ArrayList implements SettableCollector
{
    /** @var array<TValue> The values */
    private array $values;

    /**
     * Creates an ArrayList
     *
     * @param Type $valueType The type requirement for all values
     */
    public function __construct(private Type $valueType)
    {
        $this->values = [];
    }

    /**
     * Adds value to the end of the list
     *
     * @param TValue $value The value
     *
     * @return self The modified list
     */
    public function addLast(mixed $value): self
    {
        $this->throwIfValueIsWrongType($value);
        $this->values[] = $value;
        return $this;
    }

    public function get(mixed $key): mixed
    {
        $this->throwIfKeyIsWrongType($key);
        if (! $this->hasKey($key)) {
            throw new \OutOfBoundsException("Cannot get value: the index '{$key}' does not exist");
        }
        return $this->values[$key];
    }

    public function hasKey(mixed $key): bool
    {
        $this->throwIfKeyIsWrongType($key);
        return $key < count($this->values);
    }

    /**
     * Removes last value from the list
     *
     * @return self The modified list
     */
    public function removeLast(): self
    {
        unset($this->values[count($this->values) - 1]);
        return $this;
    }

    public function set(mixed $key, mixed $value): static
    {
        $this->throwIfKeyIsWrongType($key);
        $this->throwIfValueIsWrongType($value);
        if (! $this->hasKey($key - 1)) {
            throw new \OutOfBoundsException("Cannot set value: the index {$key} exceeds the ArrayList size");
        }
        $this->values[$key] = $value;
        return $this;
    }

    /**
     * Throws exception if the key is the wrong type
     */
    private function throwIfKeyIsWrongType(mixed $key): void
    {
        if (! is_int($key)) {
            throw new \InvalidArgumentException('Invalid key: the key is not an integer');
        }
    }

    /**
     * Throws exception if the value is the wrong type
     */
    private function throwIfValueIsWrongType(mixed $value): void
    {
        if (! $this->valueType->isInstance($value)) {
            throw new \InvalidArgumentException('Invalid value: the value is the wrong type');
        }
    }
}
