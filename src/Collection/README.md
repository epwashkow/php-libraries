# Collections

## ArrayList

Stores a sequential list of values by 0-based indexes.

```
/** @var ArrayList<int> */
$intList = new ArrayList(new IntegerType());

/** @var ArrayList<string> */
$stringList = new ArrayList(new StringType());
```

## StringHashMap

Maps string key => value pairs using a Hash Map.

Use the following syntax to enable generic type hinting in most IDEs.
```
/** @var StringHashMap<int> */
$intMap = new StringHashMap(new IntegerType());

/** @var StringHashMap<string> */
$stringMap = new StringHashMap(new StringType());
```