<?php

declare(strict_types=1);

namespace EvanWashkow\PhpLibraries\Collection;

use EvanWashkow\PhpLibraries\CollectionInterface\SettableCollector;
use EvanWashkow\PhpLibraries\TypeInterface\Type;

/**
 * Maps key => value pairs using a Hash Map
 *
 * @template TValue The value type
 *
 * @implements SettableCollector<string, TValue>
 */
final class StringHashMap implements SettableCollector
{
    /** @var array<TValue> */
    private array $values;

    /**
     * Creates a StringHashMap
     *
     * @param Type $valueType The type requirement for all values
     */
    public function __construct(private Type $valueType)
    {
        $this->values = [];
    }

    public function get(mixed $key): mixed
    {
        $this
            ->throwIfKeyIsWrongType($key)
            ->throwIfKeyDoesNotExist($key, 'Cannot get value: the key does not exist');
        return $this->values[$key];
    }

    public function hasKey(mixed $key): bool
    {
        $this->throwIfKeyIsWrongType($key);
        return array_key_exists($key, $this->values);
    }

    /**
     * Remove a value by its key
     *
     * @param string The key for the value
     *
     * @return self The modified map
     */
    public function removeKey(string $key): self
    {
        $this->throwIfKeyDoesNotExist($key, 'Cannot remove value: the key does not exist');
        unset($this->values[$key]);
        return $this;
    }

    public function set(mixed $key, mixed $value): static
    {
        $this->throwIfKeyIsWrongType($key);
        if (! $this->valueType->isInstance($value)) {
            throw new \InvalidArgumentException('Cannot set value: the value is the wrong type');
        }
        $this->values[$key] = $value;
        return $this;
    }

    /**
     * Throws exception if the key is the wrong type
     *
     * @return self The map
     */
    private function throwIfKeyIsWrongType(mixed $key): self
    {
        if (! is_string($key)) {
            throw new \InvalidArgumentException('Invalid key: the key is not a string');
        }
        return $this;
    }

    /**
     * Throws exception if the key does not exist with the message
     *
     * @param string $key The key to check
     * @param string $exceptionMessage The exception message if one should be thrown
     */
    private function throwIfKeyDoesNotExist(string $key, string $exceptionMessage): void
    {
        if (! $this->hasKey($key)) {
            throw new \OutOfBoundsException($exceptionMessage);
        }
    }
}
