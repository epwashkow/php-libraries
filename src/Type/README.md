# Types

Each PHP type is represented as its own Type class, which retrieves information about it.

1. `new ArrayType()` - Array Type.
2. `new BooleanType()` - Bool / Boolean Type.
3. `new FloatType()` - Float / Double Type.
4. `new IntegerType()` - Integer Type.
5. Object-Oriented Type
    * Short hand - `ObjectType::fromName(ClassOrInterface::class)`
    * Long hand - `new ObjectType(new \ReflectionClass(ClassOrInterface::class))`
6. `new StringType()` - String Type.

## See also
* [Type Interfaces](../TypeInterface)