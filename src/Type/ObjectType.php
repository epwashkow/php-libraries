<?php

declare(strict_types=1);

namespace EvanWashkow\PhpLibraries\Type;

use EvanWashkow\PhpLibraries\Equatable;
use EvanWashkow\PhpLibraries\TypeInterface\Type;

/**
 * An Object-Oriented Type (class/interface)
 */
final class ObjectType implements Type
{
    public function __construct(private \ReflectionClass $reflectionClass)
    {
    }

    /**
     * Create an ObjectType for the given class/interface name
     *
     * @throws \InvalidArgumentException For unfound type
     */
    public static function fromName(string $name): self
    {
        try {
            return new self(new \ReflectionClass($name));
        } catch (\ReflectionException $e) {
            throw new \InvalidArgumentException("Class/Interface \"{$name}\" does not exist");
        }
    }

    public function equals(Equatable $value): bool
    {
        return ($value instanceof self) && ($this->reflectionClass->getName() === $value->reflectionClass->getName());
    }

    public function isInstance(mixed $value): bool
    {
        return is_object($value) && $this->reflectionClass->isInstance($value);
    }
}
