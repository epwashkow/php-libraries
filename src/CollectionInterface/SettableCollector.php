<?php

declare(strict_types=1);

namespace EvanWashkow\PhpLibraries\CollectionInterface;

/**
 * Describes a collection for setting key => value pairs
 *
 * @template TKey The key type
 * @template TValue The value type
 */
interface SettableCollector extends KeyedCollector
{
    /**
     * Store a value at the key
     *
     * @param TKey $key The key for the value
     * @param TValue $value The value
     *
     * @return static The modified collection
     */
    public function set(mixed $key, mixed $value): static;
}
