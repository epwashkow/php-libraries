<?php

declare(strict_types=1);

namespace EvanWashkow\PhpLibraries\CollectionInterface;

/**
 * Describes a collection with key => value pairs
 *
 * @template TKey The key type
 * @template TValue The value type
 */
interface KeyedCollector
{
    /**
     * Retrieve the value at the key
     *
     * @param TKey $key The key for the value
     *
     * @return TValue The value
     */
    public function get(mixed $key): mixed;

    /**
     * Determines if the key exists
     *
     * @param TKey $key The key
     *
     * @return bool Whether or not the key exists
     */
    public function hasKey(mixed $key): bool;
}
