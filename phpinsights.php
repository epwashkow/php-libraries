<?php

declare(strict_types=1);

return [
    'preset' => 'default',
    'config' => [
        \PHP_CodeSniffer\Standards\Generic\Sniffs\Files\LineLengthSniff::class => [
            'lineLimit' => 120,
            'absoluteLineLimit' => 120,
            'ignoreComments' => false,
        ],
        \SlevomatCodingStandard\Sniffs\TypeHints\DisallowMixedTypeHintSniff::class => [
            'exclude' => [
                'src/TypeInterface/Type.php', // 'mixed' is required for isInstance()
            ],
        ],
    ],
];
