# PHP Libraries

This is a common set of libraries that bring PHP into the 21st century, creating a far more sane - and safe - development experience.

It is specifically designed to handle PHP, so you don't have to, with strict types and solid object-oriented design practices from a developer with 10+ years of experience. This means, no more indeterminate types and no more trying to track down a bunch of functions (looking at you arrays).

This library adheres to PSR-12 standards and best software design practices such as S.O.L.I.D., resulting in very flexible and maintainable code. It is also extremely well tested, following real T.D.D. strategies.

## Components
* [Collections](./src/Collection)
* [Types](./src/Type)
* [Type Interfaces](./src/TypeInterface)

## Local Dev Setup
1. Install Docker
2. In a terminal shell, run `bin/init`
3. Install VS Code Extensions
   1. [PHP](https://marketplace.visualstudio.com/items?itemName=DEVSENSE.phptools-vscode)
   2. [PHP Create Class](https://marketplace.visualstudio.com/items?itemName=jaguadoromero.vscode-php-create-class)

## Local Dev Scripts

* Composer
  * `bin/composer install`: installs packages from the versions specified in the `composer.lock` file
  * `bin/composer update`: updates packages and updates their version numbers in the `composer.lock` file
* Node
   * `bin/npm install`: install packages from the versions specified in the `package-lock.json` file
   * `bin/npm update`: updates packages in `package.json` and updates their versions numbers in the `package-lock.json` file
   * `bin/npx`: run a node script
* PHPInsights
  * `bin/phpinsights`: check code quality of `src` (default)
  * `bin/phpinsights fix`: run code fixer
  * `bin/phpinsights analyse`: check code quality
  * `bin/phpinsights --help`: help docs
* PHPUnit (Unit Test)
  * `bin/phpunit`: run tests

### Environment variables

The above scripts can be modified by specifying the following environment variables before the command:

* `DEBUG=1 <command>`: runs the command with debug output
* `PHP_VERSION=<x.y> <command>`: runs the command with a different PHP version
   * Will work for all PHP commands, except `bin/composer`

## Contribution Standards

* [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) - All commits must follow this standard. By following it, release tags will automatically be created when merging into `main`.
* [Testing Practices](docs/testing-practices.md)
