# Testing Practices

## TDD

All tests should be written following strict, by-the-book TDD.

## expectException()

Do not use `expectException()`. Instead, Extend `tests/TestCase` to use the `assertThrows()` method instead.

This built-in assertion from PHPUnit can produce false positives in tests. Example:

```php
final class ThingTest extends \PHPUnit\Framework\TestCase
{
   public function testBadMethodCallsThrowSameException(): void
   {
      $this->expectException(ThingException::class);
      $this->thing->explode();   // throws ThingException
      $this->thing->blowUp();    // did not throw, but expected to
                                 // test passes
   }
}
```

Fixed:

```php
final class ThingTest extends TestCase
{
   public function testBadMethodCallsThrowSameException(): void
   {
      $this->assertThrows(
         ThingException::class,
         fn () => $this->thing->explode()    // throws ThingException
      );
      $this->assertThrows(
         ThingException::class,
         fn () => $this->thing->blowUp()     // did not throw, but expected to
      );
                                             // test fails
   }
}
```

## Do not share tests

There is a temptation to share test cases. Do not do so. All tests should be written following strict TDD. Writing
shared test suites for classes violates the iterative, incremental development process of TDD that forces the correct
solution to emerge.

Example of what not to do:

```php
final class ThingATest extends BaseThingTestCase
{
   protected function createThing(): Thing
   {
      return new ThingB();
   }
}

final class ThingBTest extends BaseThingTestCase
{
   protected function createThing(): Thing
   {
      return new ThingB();
   }
}

abstract class BaseThingTestCase
{
   abstract protected function createThing(): Thing;

   public function testDoNothingThrowsException(): void
   {
      $thing = createThing();
      $this->expectException(ThingException::class);
      $thing->doNothing();
   }
}
```

## Shared assertions

Not sharing tests does not prohibit sharing assertions, which can be useful to guarentee interface contracts.

Example of what to do:

```php
final class ThingATest extends BaseThingTestCase
{
   public function testDoNothingThrowsException(): void
   {
      $this->assertThrowsExceptionOnDoNothing();
      $this->thing->doNothing();
   }
}

final class ThingBTest extends BaseThingTestCase
{
   public function testDoNothingThrowsException(): void
   {
      $this->assertThrowsExceptionOnDoNothing();
      $this->thing->doNothing();
   }
}

abstract class BaseThingTestCase
{
   final protected function assertThrowsExceptionOnDoNothing(): void
   {
      $this->expectException(ThingException::class);
   }
}
```
