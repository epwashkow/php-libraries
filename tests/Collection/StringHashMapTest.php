<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\Collection;

use EvanWashkow\PhpLibraries\Collection\StringHashMap;
use EvanWashkow\PhpLibraries\CollectionInterface\SettableCollector;
use EvanWashkow\PhpLibraries\TypeInterface\Type;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\TestWith;
use PHPUnit\Framework\MockObject\Stub;

final class StringHashMapTest extends CollectionTestCase
{
    private StringHashMap $map;
    private Type|Stub $valueType;

    protected function setUp(): void
    {
        $this->valueType = $this->createStub(Type::class);
        $this->map = new StringHashMap($this->valueType);
    }

    public function testImplementsSettableCollector(): void
    {
        $this->assertInstanceOf(SettableCollector::class, $this->map);
    }

    /**
     * Wrong key types
     *
     * get()
     * hasKey()
     * set()
     */

    #[DataProvider('wrongKeyTypeProvider')]
    public function testGetThrowsOnWrongKeyType(mixed $key): void
    {
        $this->assertThrowsOnWrongType(fn () => $this->map->get($key));
    }

    #[DataProvider('wrongKeyTypeProvider')]
    public function testHasKeyThrowsOnWrongKeyType(mixed $key): void
    {
        $this->assertThrowsOnWrongType(fn () => $this->map->hasKey($key));
    }

    #[DataProvider('wrongKeyTypeProvider')]
    public function testSetThrowsOnWrongKeyType(mixed $key): void
    {
        $this->stubValueType(true);
        $this->assertThrowsOnWrongType(fn () => $this->map->set($key, 'foobar'));
    }

    public static function wrongKeyTypeProvider(): array
    {
        return [[0], [2.5]];
    }

    /**
     * Wrong value types
     *
     * set()
     */

    #[TestWith([4])]
    #[TestWith([21])]
    public function testSetThrowsOnWrongValueType(mixed $value): void
    {
        $this->stubValueType(false);
        $this->assertThrowsOnWrongType(fn () => $this->map->set('0', $value));
    }

    /**
     * Accessing key that does not exist
     *
     * get()
     * hasKey()
     * removeKey()
     */

    #[TestWith([[], '36'])]
    #[TestWith([[], '77'])]
    #[TestWith([[new KeyValuePair('490', 21)], '87'])]
    public function testAccessingKeysThatDoNotExist(array $initialKeyValues, string $missingKey): void
    {
        $this->initializeMap(...$initialKeyValues);
        $this->assertMapKeyNotExists($missingKey);
    }

    /**
     * Returns same StringHashMap instance
     *
     * removeKey()
     * set()
     */

    public function testRemoveKeyReturnsSameStringHashMap(): void
    {
        $this->stubValueType(true);
        $this->map->set('0', 0);

        $gotMap = $this->map->removeKey('0');
        $this->assertSame($this->map, $gotMap);
    }

    public function testSetReturnsSameStringHashMap(): void
    {
        $this->stubValueType(true);

        $gotMap = $this->map->set('0', 0);
        $this->assertSame($this->map, $gotMap);
    }

    /**
     * removeKey()
     */

    #[TestWith([[new KeyValuePair('67', 81)], '67', []])]
    #[TestWith([
        [new KeyValuePair('22', 16), new KeyValuePair('4', 81)],
        '4',
        [new KeyValuePair('22', 16)],
    ])]
    #[TestWith([
        [new KeyValuePair('27', 81), new KeyValuePair('-52', -23), new KeyValuePair('foo', 45)],
        '27',
        [new KeyValuePair('-52', -23), new KeyValuePair('foo', 45)],
    ])]
    public function testRemoveKey(array $initialKeyValues, string $keyToRemove, array $expectedRemainingKeyValues): void
    {
        $this->initializeMap(...$initialKeyValues);
        $this->map->removeKey($keyToRemove);

        $this->assertMapKeyNotExists($keyToRemove);
        $this->assertCollectionContains($this->map, ...$expectedRemainingKeyValues);
    }

    /**
     * set()
     */

    #[TestWith([new KeyValuePair('42', 130)])]
    #[TestWith([new KeyValuePair('90', 110), new KeyValuePair('278', 12)])]
    #[TestWith([new KeyValuePair('-78', 0),  new KeyValuePair('86', 846)])]
    public function testSet(KeyValuePair ...$keyValuePairs): void
    {
        $this->initializeMap(...$keyValuePairs);
        $this->assertCollectionContains($this->map, ...$keyValuePairs);
    }

    /**
     * Assertions
     */

    private function assertMapKeyNotExists(string $missingKey): void
    {
        $this->assertCollectionKeyNotExists($this->map, $missingKey);

        $this->assertThrows(
            \OutOfBoundsException::class,
            fn () => $this->map->removeKey($missingKey)
        );
    }

    /**
     * Utilities
     */

    /**
     * Initialize $this->map with key => values
     *
     * @param array<KeyValuePair> $keyValues
     */
    private function initializeMap(KeyValuePair ...$keyValues): void
    {
        $this->stubValueType(true);
        foreach ($keyValues as $keyValuePair) {
            $this->map->set($keyValuePair->getKey(), $keyValuePair->getValue());
        }
    }

    private function stubValueType(bool $isInstance): void
    {
        $this->valueType->method('isInstance')->willReturn($isInstance);
    }
}
