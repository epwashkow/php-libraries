<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\Collection;

use EvanWashkow\PhpLibraries\CollectionInterface\KeyedCollector;
use Tests\EvanWashkow\PhpLibraries\TestCase;

abstract class CollectionTestCase extends TestCase
{
    /**
     * Assert a collection contains the array of key-value pairs
     *
     * This method does not use an array of [key => value] pair mappings because PHP implicitly casts keys.
     * Source: https://www.php.net/manual/en/language.types.array.php
     *
     * @param KeyedCollector $collection The collection to test
     * @param array<KeyValuePair> $keyValuePairs The Key-Value Pairs to assert are in the collection
     */
    final protected function assertCollectionContains(KeyedCollector $collection, KeyValuePair ...$keyValuePairs): void
    {
        foreach ($keyValuePairs as $keyValuePair) {
            $this->assertCollectionKeyExists($collection, $keyValuePair->getKey(), $keyValuePair->getValue());
        }
    }

    final protected function assertCollectionKeyExists(KeyedCollector $collection, mixed $key, mixed $expectedValue): void
    {
        $this->assertTrue(
            $collection->hasKey($key),
            'Collection->hasKey() was expected to return true, but returned false'
        );
        $this->assertSame(
            $expectedValue,
            $collection->get($key),
            'Collection->get() did not return the expected value'
        );
    }

    final protected function assertCollectionKeyNotExists(KeyedCollector $collection, mixed $key): void
    {
        $this->assertFalse(
            $collection->hasKey($key),
            'Collection->hasKey() was expected to return false, but returned true'
        );
        $this->assertThrows(
            \OutOfBoundsException::class,
            static fn () => $collection->get($key)
        );
    }

    final protected function assertThrowsOnWrongType(\Closure $fn): void
    {
        $this->assertThrows(\InvalidArgumentException::class, $fn);
    }
}
