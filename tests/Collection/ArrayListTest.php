<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\Collection;

use EvanWashkow\PhpLibraries\Collection\ArrayList;
use EvanWashkow\PhpLibraries\CollectionInterface\SettableCollector;
use EvanWashkow\PhpLibraries\TypeInterface\Type;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\TestWith;
use PHPUnit\Framework\MockObject\Stub;

final class ArrayListTest extends CollectionTestCase
{
    private ArrayList $list;
    private Type|Stub $valueType;

    protected function setUp(): void
    {
        $this->valueType = $this->createStub(Type::class);
        $this->list = new ArrayList($this->valueType);
    }

    public function testImplementsSettableCollector(): void
    {
        $this->assertInstanceOf(SettableCollector::class, $this->list);
    }

    /**
     * Wrong key types
     *
     * get()
     * hasKey()
     * set()
     */

    #[DataProvider('wrongKeyTypeProvider')]
    public function testGetThrowsOnWrongKeyType(mixed $key): void
    {
        $this->assertThrowsOnWrongType(fn () => $this->list->get($key));
    }

    #[DataProvider('wrongKeyTypeProvider')]
    public function testHasKeyThrowsOnWrongKeyType(mixed $key): void
    {
        $this->assertThrowsOnWrongType(fn () => $this->list->hasKey($key));
    }

    #[DataProvider('wrongKeyTypeProvider')]
    public function testSetThrowsOnWrongKeyType(mixed $key): void
    {
        $this->stubValueType(true);
        $this->assertThrowsOnWrongType(fn () => $this->list->set($key, 1));
    }

    public static function wrongKeyTypeProvider(): array
    {
        return [
            ['foobar'],
            [0.2],
        ];
    }

    /**
     * Wrong value types
     *
     * addLast()
     * set()
     */

    public function testAddLastThrowsOnWrongValueType(): void
    {
        $this->stubValueType(false);
        $this->assertThrowsOnWrongType(fn () => $this->list->addLast(0));
    }

    public function testSetThrowsOnWrongValueType(): void
    {
        $this->stubValueType(false);
        $this->assertThrowsOnWrongType(fn () => $this->list->set(0, 0));
    }

    /**
     * Accessing key that does not exist
     *
     * get()
     * hasKey()
     */

    #[TestWith([[], 0])]
    #[TestWith([[37], 1])]
    #[TestWith([[37], 2])]
    public function testAccessingKeysThatDoNotExist(array $initialValues, int $missingKey): void
    {
        $this->initializeList($initialValues);
        $this->assertCollectionKeyNotExists($this->list, $missingKey);
    }

    /**
     * Returns same ArrayList instance
     *
     * addLast()
     * removeLast()
     * set()
     */

    public function testAddLastReturnsSameArrayList(): void
    {
        $this->stubValueType(true);

        $list = $this->list->addLast(1);
        $this->assertSame($list, $this->list);
    }

    public function testRemoveLastReturnsSameArrayList(): void
    {
        $this->stubValueType(true);

        $list = $this->list->removeLast();
        $this->assertSame($list, $this->list);
    }

    public function testSetReturnsSameArrayList(): void
    {
        $this->stubValueType(true);

        $list = $this->list->set(0, 0);
        $this->assertSame($list, $this->list);
    }

    /**
     * addLast()
     */

    #[TestWith([[44]])]
    #[TestWith([[16]])]
    #[TestWith([[72, 54]])]
    public function testAddLast(array $values): void
    {
        $this->stubValueType(true);

        foreach ($values as $value) {
            $this->list->addLast($value);
        }

        $this->assertCollectionContains($this->list, ...$this->toKeyValuePairs($values));
    }

    /**
     * removeLast()
     */

    #[TestWith([[25, 28, 86], 1, [25, 28]])]
    #[TestWith([[25, 28, 86], 2, [25]])]
    public function testRemoveLastStillHasOtherKeys(array $initialValues, int $numRemoveLast, array $expected): void
    {
        $this->initializeList($initialValues);
        $this->removeLastNTimes($numRemoveLast);

        $this->assertCollectionContains($this->list, ...$this->toKeyValuePairs($expected));
    }

    #[TestWith([[],         1, 0])]
    #[TestWith([[67],       1, 0])]
    #[TestWith([[1, 2, 3],  2, 1])]
    public function testRemoveLastRemovesKeys(array $initialValues, int $numRemoveLast, int $missingKey): void
    {
        $this->initializeList($initialValues);
        $this->removeLastNTimes($numRemoveLast);

        $this->assertCollectionKeyNotExists($this->list, $missingKey);
    }

    public function removeLastNTimes(int $numRemoveLast): void
    {
        for ($i = 0; $i < $numRemoveLast; $i++) {
            $this->list->removeLast();
        }
    }

    /**
     * set()
     */

    #[TestWith([[],     1])]
    #[TestWith([[1],    2])]
    #[TestWith([[1],    3])]
    public function testSetThrowsOutOfBoundsExceptionOnKeysOutsideRange(array $initialValues, int $key): void
    {
        $this->initializeList($initialValues);
        $this->assertThrows(
            \OutOfBoundsException::class,
            fn () => $this->list->set($key, 0)
        );
    }

    #[TestWith([[],     0, 76,  [76]])]
    #[TestWith([[23],   1, 102, [23, 102]])]
    #[TestWith([[76],   0, 102, [102]])]
    public function testSet(array $initialValues, int $keyToSet, int $valueToSet, array $expected): void
    {
        $this->initializeList($initialValues);

        $this->list->set($keyToSet, $valueToSet);
        $this->assertCollectionContains($this->list, ...$this->toKeyValuePairs($expected));
    }

    /**
     * Utilities
     */

    private function initializeList(array $values): void
    {
        $this->stubValueType(true);
        foreach ($values as $value) {
            $this->list->addLast($value);
        }
    }

    private function stubValueType(bool $isInstance): void
    {
        $this->valueType->method('isInstance')->willReturn($isInstance);
    }

    /**
     * Converts a flat array to a KeyValuePair array
     *
     * @param array<mixed> $values The values
     *
     * @return array<KeyValuePair>
     */
    private function toKeyValuePairs(array $values): array
    {
        $keyValuePairs = [];
        foreach ($values as $key => $value) {
            $keyValuePairs[] = new KeyValuePair($key, $value);
        }
        return $keyValuePairs;
    }
}
