<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\Collection;

final class KeyValuePair
{
    public function __construct(private mixed $key, private mixed $value)
    {
    }

    public function getKey(): mixed
    {
        return $this->key;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }
}
