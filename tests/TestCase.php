<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * Assert that a callback function throws an exception
     *
     * Unlike expectException(), this method only checks for exceptions within the callback function.
     *
     * @param string $exception The class name of the exception (i.e. \Exception::class)
     * @param \Closure $fn The callback function
     */
    final protected function assertThrows(string $exception, \Closure $fn): void
    {
        $caughtException = null;
        try {
            $fn();
        } catch (\Throwable $e) {
            $caughtException = $e::class;
        }

        $this->assertSame($exception, $caughtException);
    }
}
