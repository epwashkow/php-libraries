<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\Type;

use EvanWashkow\PhpLibraries\Type\FloatType;
use EvanWashkow\PhpLibraries\TypeInterface\Type;
use PHPUnit\Framework\Attributes\TestWith;
use Tests\EvanWashkow\PhpLibraries\TestCase;

final class FloatTypeTest extends TestCase
{
    private FloatType $type;

    protected function setUp(): void
    {
        $this->type = new FloatType();
    }

    public function testImplementsType(): void
    {
        $this->assertInstanceOf(Type::class, $this->type);
    }

    public function testNotEqualsType(): void
    {
        $mockType = $this->createStub(Type::class);
        $this->assertFalse($this->type->equals($mockType));
    }

    public function testEqualsFloatType(): void
    {
        $this->assertTrue($this->type->equals(new FloatType()));
    }

    #[TestWith([true, false])]
    #[TestWith([1, false])]
    #[TestWith([1.3, true])]
    #[TestWith([9.0, true])]
    public function testIsInstance(mixed $value, bool $expected): void
    {
        $this->assertSame($expected, $this->type->isInstance($value));
    }
}
