<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\Type;

use EvanWashkow\PhpLibraries\Type\StringType;
use EvanWashkow\PhpLibraries\TypeInterface\Type;
use PHPUnit\Framework\Attributes\TestWith;
use Tests\EvanWashkow\PhpLibraries\TestCase;

final class StringTypeTest extends TestCase
{
    private StringType $type;

    protected function setUp(): void
    {
        $this->type = new StringType();
    }

    public function testImplementsType(): void
    {
        $this->assertInstanceOf(Type::class, $this->type);
    }

    public function testNotEqualsType(): void
    {
        $mockType = $this->createStub(Type::class);
        $this->assertFalse($this->type->equals($mockType));
    }

    public function testEqualsStringType(): void
    {
        $this->assertTrue($this->type->equals(new StringType()));
    }

    #[TestWith([true, false])]
    #[TestWith([1, false])]
    #[TestWith(['', true])]
    #[TestWith(['foobar', true])]
    public function testIsInstance(mixed $value, bool $expected): void
    {
        $this->assertSame($expected, $this->type->isInstance($value));
    }
}
