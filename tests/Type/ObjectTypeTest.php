<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\Type;

use EvanWashkow\PhpLibraries\Type\ObjectType;
use EvanWashkow\PhpLibraries\TypeInterface\Type;
use PHPUnit\Framework\Attributes\TestWith;
use Tests\EvanWashkow\PhpLibraries\TestCase;

final class ObjectTypeTest extends TestCase
{
    private ObjectType $type;

    protected function setUp(): void
    {
        $this->type = $this->createObjectType('');
    }

    public function testImplementsType(): void
    {
        $this->assertInstanceOf(Type::class, $this->type);
    }

    // fromName()
    public function testFromNameThrowsException(): void
    {
        $this->assertThrows(
            \InvalidArgumentException::class,
            static fn () => ObjectType::fromName('foobar')
        );
    }

    #[TestWith([\Exception::class])]
    #[TestWith([\Iterator::class])]
    public function testFromName(string $typeName): void
    {
        $expected = new ObjectType(new \ReflectionClass($typeName));
        $objectType = ObjectType::fromName($typeName);

        $this->assertTrue($objectType->equals($expected));
    }

    // equals()
    public function testNotEqualsType(): void
    {
        $mockType = $this->createStub(Type::class);
        $this->assertFalse($this->type->equals($mockType));
    }

    #[TestWith(['foo\bar', 'foo\bar', true])]
    #[TestWith(['lorem\ipsum', 'lorem\ipsum', true])]
    #[TestWith(['foo\bar', 'lorem\ipsum', false])]
    #[TestWith(['lorem\ipsum', 'foo\bar', false])]
    public function testEquals(string $typeNameA, string $typeNameB, bool $expected): void
    {
        $objectTypeA = $this->createObjectType($typeNameA);
        $objectTypeB = $this->createObjectType($typeNameB);

        $this->assertSame($expected, $objectTypeA->equals($objectTypeB));
    }

    // isInstance()
    #[TestWith([false])]
    #[TestWith([1])]
    public function testIsInstanceWithPrimitiveValues(mixed $value): void
    {
        $this->assertFalse($this->type->isInstance($value));
    }

    #[TestWith([true, true])]
    #[TestWith([false, false])]
    public function testIsInstanceWithObjectValues(bool $isInstanceMockValue, bool $expected): void
    {
        $reflection = $this->createStub(\ReflectionClass::class);
        $reflection->method('isInstance')->willReturn($isInstanceMockValue);

        $type = new ObjectType($reflection);
        $object = new \stdClass();

        $this->assertSame($expected, $type->isInstance($object));
    }

    // Utilities
    private function createObjectType(string $typeName): ObjectType
    {
        $reflection = $this->createStub(\ReflectionClass::class);
        $reflection->method('getName')->willReturn($typeName);

        return new ObjectType($reflection);
    }
}
