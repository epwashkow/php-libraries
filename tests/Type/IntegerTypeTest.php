<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\Type;

use EvanWashkow\PhpLibraries\Type\IntegerType;
use EvanWashkow\PhpLibraries\TypeInterface\Type;
use PHPUnit\Framework\Attributes\TestWith;
use Tests\EvanWashkow\PhpLibraries\TestCase;

final class IntegerTypeTest extends TestCase
{
    private IntegerType $type;

    protected function setUp(): void
    {
        $this->type = new IntegerType();
    }

    public function testImplementsType(): void
    {
        $this->assertInstanceOf(Type::class, $this->type);
    }

    public function testNotEqualsType(): void
    {
        $mockType = $this->createStub(originalClassName: Type::class);
        $this->assertFalse($this->type->equals($mockType));
    }

    public function testEqualsIntegerType(): void
    {
        $this->assertTrue($this->type->equals(new IntegerType()));
    }

    #[TestWith([true, false])]
    #[TestWith([[], false])]
    #[TestWith([1, true])]
    #[TestWith([82, true])]
    public function testIsInstance(mixed $value, bool $expected): void
    {
        $this->assertSame($expected, $this->type->isInstance($value));
    }
}
