<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\Type;

use EvanWashkow\PhpLibraries\Type\BooleanType;
use EvanWashkow\PhpLibraries\TypeInterface\Type;
use PHPUnit\Framework\Attributes\TestWith;
use Tests\EvanWashkow\PhpLibraries\TestCase;

final class BooleanTypeTest extends TestCase
{
    private BooleanType $type;

    protected function setUp(): void
    {
        $this->type = new BooleanType();
    }

    public function testImplementsType(): void
    {
        $this->assertInstanceOf(Type::class, $this->type);
    }

    public function testNotEqualsType(): void
    {
        $mockType = $this->createStub(Type::class);
        $this->assertFalse($this->type->equals($mockType));
    }

    public function testEqualsBooleanType(): void
    {
        $this->assertTrue($this->type->equals(new BooleanType()));
    }

    #[TestWith([1, false])]
    #[TestWith([1.4, false])]
    #[TestWith([true, true])]
    #[TestWith([false, true])]
    public function testIsInstance(mixed $value, bool $expected): void
    {
        $this->assertSame($expected, $this->type->isInstance($value));
    }
}
