<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\TypeInterface;

use EvanWashkow\PhpLibraries\Equatable;
use EvanWashkow\PhpLibraries\TypeInterface\Type;
use Tests\EvanWashkow\PhpLibraries\TestCase;

final class TypeTest extends TestCase
{
    public function testImplementsEquatable(): void
    {
        $typeMock = $this->createMock(Type::class);
        $this->assertInstanceOf(Equatable::class, $typeMock);
    }
}
