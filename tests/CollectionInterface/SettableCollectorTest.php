<?php

declare(strict_types=1);

namespace Tests\EvanWashkow\PhpLibraries\CollectionInterface;

use EvanWashkow\PhpLibraries\CollectionInterface\KeyedCollector;
use EvanWashkow\PhpLibraries\CollectionInterface\SettableCollector;
use Tests\EvanWashkow\PhpLibraries\TestCase;

final class SettableCollectorTest extends TestCase
{
    public function testImplementsKeyedCollector(): void
    {
        $collection = $this->createStub(SettableCollector::class);
        $this->assertInstanceOf(KeyedCollector::class, $collection);
    }
}
